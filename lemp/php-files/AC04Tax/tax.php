<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
        <?php
          
            include 'valid.php'
        ?>
    <div  class="container mt-3">
    <h1>PRICE, TAX and ROUNDS</h1>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
        <div  class="mb-3 mt-3 " >
            Price with TAX: 
        <br>
            <div class="row">
             <div class=col-md-6> 
        <input type="integer" class="form-control" name="price" required>
        </div>
        <div class=col-auto> 
                <span>* <?php echo $priceErr ?></span>
        </div>
        </div>
        <br>
        TAX (%): 
        <br>
        <div class="row">
        <div class=col-md-6> 
            <input type="integer"  class="form-control" name="tax"  required><br><br>
            <input type="submit"  class="btn btn-primary" name="execute" value="CALCULATE">
        </div>
        <div class=col-auto> 
                <span >*<?php echo $taxErr ?></span>
        </div>
        </div>
      
        <?php
            include 'codi.php';
        ?>
        </div>
    </div>
</body>
</html>