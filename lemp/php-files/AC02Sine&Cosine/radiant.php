<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<head>
    <link rel="stylesheet" href="style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
    <h1>Sine & Cosine</h1>
    <img src="sinus_cosinus.png"></img>
    <table>
        <th>Degrees</th>
        <th>Radiant</th>
        <th>Sine</th>
        <th>Cosine</th>
        <?php
            include 'codi.php';
        ?>
    </table>
</body>