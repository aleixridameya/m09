
<?php




for ($degrees=0; $degrees <=360 ; $degrees++) { 
    $valor_sin = "positive";
    $valor_cos = "positive" ;
    
    echo "<tr><td>$degrees</td>";

    $radiant = round(deg2rad($degrees),4);
    echo "<td>$radiant</td>";

    $sine = round(sin($radiant),4);
    
    $cosine = round(cos($radiant),4);
    
    if ($sine < 0) {
        $valor_sin = "negative";
    }

    if ($cosine < 0) {
       $valor_cos = "negative";
    }

    echo "<td class='$valor_sin'>$sine</td>";
    echo "<td class='$valor_cos'>$cosine</td></tr>";
    

}

?>