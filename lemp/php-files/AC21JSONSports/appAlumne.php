<?php

//Get all countrys JSON file
$url = "https://www.thesportsdb.com/api/v1/json/3/all_countries.php"; // path to your JSON file
$response = file_get_contents($url); // Obtener el JSON como string

//Get JSON object with json_decode() without associative array
$data = json_decode($response, true); // Decodificar el JSON a un objeto PHP

$eleccio = isset($_POST['country']) ? $_POST['country'] : '';
//Get array "countries" from JSON object
//$array = ???;

//Create form: "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>"
echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";
echo "<div class='row'>";
echo "<div class='col-md-6'>";
//Add select field: "<select>"
echo "<select name='country' class='form-control'>";

//Iterating $array
foreach ($data['countries'] as $country) {
  $pais = $country['name_en'];
  $selected = ($eleccio == $pais) ? 'selected' : '';
//Add all elements of array to <option></option>
  echo "<option value='$pais' $selected>$pais</option>";
  }

//Add  "</select>"
echo "</select>";
echo "</div>";
echo "<div class='col-md-6'>";
//Add <input type='submit' name='execute'/>
echo "<button class='btn btn-primary' type='submit' name='execute'>Select</button>";
echo "</div>";
echo "</div>";
//Add </form>
echo "</form>";


//if $_SERVER['REQUEST_METHOD'] === 'POST' and 
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['execute'])) {
  execute($_POST['country']);
}

// isset isset($_POST['execute']), then call execute($_POST['country']) method


function execute($country)
{
  
  $url = "https://www.thesportsdb.com/api/v1/json/3/search_all_leagues.php?c=" . $country; // path to your JSON file
  //Get JSON object with json_decode() with associative array

  $response = file_get_contents($url); 
  $data = json_decode($response, true);

 
  //Add <table>
  echo "<table class='table table-striped'>";  //Add <thead>
  echo "<thead class='table-primary' style='text-align:center'>";
  //Add <th> .. </th>
  echo "<tr>";
  echo "<th>Flag</th>";
  echo "<th>Title</th>";
  echo "<th>Website</th>";
  echo "<th>Image</th>";
  echo "</tr>";
  //Add <tbody>
  echo "</thead>";
  echo "<tbody style='text-align:center'>";
 
  //Get array "countries" from JSON object
  //$array = ???;

  if (isset($data['countries'])) {
    foreach ($data['countries'] as $lliga) {
      echo "<tr>";
        echo "<td><img src='{$lliga['strBadge']}' alt='{$country}' width='100'></td>";
        echo "<td>{$lliga['strLeague']}</td>";
        echo "<td>";

          if (!empty($lliga['strWebsite'])) {
            echo "<a href=https://{$lliga['strWebsite']}'>https://{$lliga['strWebsite']}</a>";
          } else {
            echo "No hi ha pàgina web";
          }
        
        echo "</td>";
        echo "<td>";

            if (!empty($lliga['strFanart1'])) {
              echo "<a href='{$lliga['strFanart1']}' target='_blank'><img src='{$lliga['strFanart1']}' alt='{$lliga['strLeague']}' width='300' class='rounded'></a>";
            } else {
              echo "No hi ha imatge";
            }
       
        echo "</td>";
        echo "</tr>";
        echo "<tr>";
              echo "<td colspan='4' style='text-align: justify;'>{$lliga['strDescriptionEN']}</td>";
          echo "</tr>";
    }
  }
  echo "</tbody>";
echo "</table>";
}


