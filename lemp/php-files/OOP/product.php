<?php
    class Product
    {
        // Properties

        private int $id;

        private string $name;

        private float $price;

        private array $colors;

        // Constructor

        function __construct(int $id,string $name,float $price, array $colors)
        {
            $this -> id = $id;
            $this -> name = $name;
            $this -> price = $price;
            $this -> colors = $colors;
        }

        // Destructor (Optional)

        function __destruct()
        {
            //unset($this); 
        }

        // Getters and setters

        function get_id(): int {
            return $this->id;
        }

        function set_id(int $id): void {
            $this->id = $id;
        }

        function get_name(): string {
            return $this -> name;
        }

        function set_name(string $name): void {
            $this -> name = $name;
        }

        function get_price(): float {
            return $this -> price;
        }

        function set_price(float $price): void {
            $this -> price = $price;
        }

        function get_colors(): array {
            return $this -> colors;
        }

        function set_color(array $colors): void {
            $this -> colors = $colors;
        }


        // Methods (Optional)

        function tax(float $tax): float {
            return $this->price * $tax / (1 + $tax);        
        }

        public function priceNoTax(float $tax): float {
             return $this->price / (1 + $tax);
        }
        
        // ToString (Optional)
        function __toString(): string {
            $text="";
            foreach ($this->colors as $color){
                $text .= $color . " ";
            }
            return "Product[id=" . $this->id . ", name" . $this->name .  ", price=" . $this-> price . ", colors= " . $text . "]";
        }




    }

$colors = ["Red", "Blue", "Green"];
$p1 = new Product(1, "Basic T-shirt", 12.55, $colors);
$p2 = new Product(2, "Long T-shirt", 10.75, ["Black"]);


$p1->get_id() . "<br>";
$p1->get_name() . "<br>";
$p1->get_price() . "<br>";

$array_colors = $p1->get_colors();
print_r($array_colors);
echo "<br>";

var_dump($array_colors);
echo "<br>";


// Get_colors() option3
echo "Colors: ";
foreach ($colors as $color) 
{
    echo $color . " ";
}
echo "<br>";

// Get_colors() option4
echo "Colors: ";
echo implode(',', $p2->get_colors());
echo "<br>";

?>

<?php

echo "Satters: <br>";
$p1->set_id(10);
echo $p1->get_name() . "<br>";

$p1->set_price(15.75);
echo $p1->get_price() . "<br>";
echo "<br />";

?>

<?php

echo "<br><br>";
echo "Methods: <br />";
echo $p1 -> priceNoTax(0.21)  . "<br />";
echo $p1 -> tax(0.21) . " <br />";

echo $p1->__toString();
?>

<?php

echo "Product Array: <br>";
$products=[];
$products[]=$p1; //array_push($products,$p1)
$products[]=$p2; 

foreach ($products as $product){
    echo $product->__toString() . "<br />";
}

?>