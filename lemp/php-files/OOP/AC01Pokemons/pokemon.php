<?php

class Pokemon {
    //Properties
    private int $code;

    private string $name;

    private string $type1;

    private string $type2;

    private int $healthPoints;

    private int $attack;

    private int $defense;

    private int $specialAttack;

    private int $specialDefense;

    private int $speed;

    private int $generation;

    private bool $legendary;

    private string $image;

    private int $total;


    //Constructor
    function __construct(
        int $code, 
        string $name,
        string $type1,
        string $type2,
        int $healthPoints,
        int $attack,
        int $defense,
        int $specialAttack,
        int $specialDefense,
        int $speed,
        int $generation,
        bool $legendary,
        string $image
    )
    {
        $this->code = $code;
        $this->name = $name;
        $this->type1 = $type1;
        $this->type2 = $type2;
        $this->healthPoints = $healthPoints;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->specialAttack = $specialAttack;
        $this->specialDefense = $specialDefense;
        $this->speed = $speed;
        $this->generation = $generation;
        $this->legendary = $legendary;
        $this->image = $image;
        $this->total = $this->total();
    }

    // Getters and setters
    function getCode(): int
    {
        return $this->code;
    }
    function setCode(int $code): void {
        $this->code = $code;
    }
    function getName(): string
    {
        return $this->name;
    }
    function setName(string $name): void {
        $this->name = $name;
    }
    function getType1(): string
    {
        return $this->type1;
    }
    function setType1(string $type1): void {
        $this->type1 = $type1;
    }
    function getType2(): string
    {
        return $this->type2;
    }
    function setType2(string $type2): void
    {
        $this->type2 = $type2;
    }
    function getHealthPoints(): int
    {
        return $this->healthPoints;
    }
    function setHealthPoints(int $healthPoints): void {
        $this->healthPoints = $healthPoints;
    }
    function getAttack(): int
    {
        return $this->attack;
    }
    function setAttack(int $attack): void {
        $this->attack = $attack;
    }
    function getdefense(): int
    {
        return $this->defense;
    }
    function setdefense(int $defense): void {
        $this->defense = $defense;
    }
    function getSpecialAttack(): int
    {
        return $this->specialAttack;
    }
    function setSpecialAttack(int $specialAttack): void {
        $this->specialAttack = $specialAttack;
    }
    function getSpecialDefense(): int
    {
        return $this->specialDefense;
    }
    function setSpecialDefense(int $specialDefense): void {
        $this->specialDefense = $specialDefense;
    }
    function getSpeed(): int
    {
        return $this->speed;
    }
    function setSpeed(int $speed): void {
        $this->speed = $speed;
    }
    function getGeneration(): int
    {
        return $this->generation;
    }
    function setGeneration(int $generation): void {
        $this->generation = $generation;
    }
    function getlegendary(): bool
    {
        return $this->legendary;
    }
    function setlegendary(bool $legendary): void {
        $this->legendary = $legendary;
    }
    function getImage(): string
    {
        return $this->image;
    }
    function setImage(string $image): void {
        $this->image = $image;
    }
    function getTotal(): int
    {
        return $this->total;
    }
    function setTotal(int $total): void {
        $this->total = $total;
    }
    
    //toString Method

    function __toString() {
        return "Pokemon:\n" .
            "Code: " . $this->getCode() .
            "Name: " . $this->getName() .
            "Type 1: " . $this->getType1() .
            "Type 2: " . $this->getType2() .
            "Health Points: " . $this->getHealthPoints() .
            "Attack: " . $this->getAttack() .
            "Defense: " . $this->getDefense() .
            "Special Attack: " . $this->getSpecialAttack() .
            "Special Defense: " . $this->getSpecialDefense() .
            "Speed: " . $this->getSpeed() .
            "Generation: " . $this->getGeneration() .
            "Legendary: " . $this->getLegendary() .
            "Image: " . $this->getImage() .
            "Total: " . $this->total();
}

    // Methods (Optional)
    function total(): int {
        return $this->healthPoints + 
               $this->attack + 
               $this->defense + 
               $this->specialAttack + 
               $this->specialDefense + 
               $this->speed;        
    }

}


?>