<?php

class Pokemons {
    // Properties
    private $pokemons;

    // Constructor
    function __construct() {
        $this->pokemons = array();
    }

    // Getters
    function getPokemons(): array {
        return $this->pokemons;
    }

    // Setters
    function setPokemons($pokemons) {
        $this->pokemons = $pokemons;
    }

    // Add Method
    function add_pokemon($pokemon) {
        $this->pokemons[] = $pokemon;
    }
    
    function get_generation(int $generation) {

        $generation_pokemons = array();

        foreach ($this->pokemons as $pokemon) {
            if ($pokemon->getGeneration() === $generation) {
                $generation_pokemons[] = $pokemon;
            }
        }
        return $generation_pokemons;

    }
    
    
    function get_pokemon_name(string $text) {
        $text = strtolower($text);
        $matching_pokemons = array();
        
        foreach ($this->pokemons as $pokemon) {
            if (strpos(strtolower($pokemon->getName()), $text) !== false) {
                $matching_pokemons[] = $pokemon;
            }
        }
        return $matching_pokemons;

    }
    
    function get_pokemon_type(string $text) {
        $text = strtolower($text); 
        $matching_pokemons = array();

        foreach ($this->pokemons as $pokemon) {
            if (strpos(strtolower($pokemon->getType1()), $text) !== false || strpos(strtolower($pokemon->getType2()), $text) !== false) {
                $matching_pokemons[] = $pokemon;
            }
        }
        return $matching_pokemons;
    }
}
?>