<?php
include_once("card.php");
include_once("hand.php");
include_once("cardPile.php");

$hand = new Hand();

if ($_POST) {
  if (isset($_POST['execute'])) {
    $cardPile = generateCards();
    $hand = getCards($cardPile);
    showCards($hand);
    $text = getPoints($hand);
    showPoints($text);
    $total = getTotal($hand);
    showTotal($total);
    evaluate($hand);
  }
}

function generateCards(): CardPile
{
  $cardPile = new CardPile();
    for ($i = 1; $i <= 13; $i++) {
        for ($j = 1; $j <= 4; $j++) {
            $card = new Card($i, $j, "imatges/card" . ($i + ($j - 1) * 13) . ".gif");
            $cardPile->addCard($card);
        }
    }
    $extraCard = new Card(55, 1, "imatges/card_55.gif");
    $cardPile->addCard($extraCard);

    return $cardPile;

}
function getCards(CardPile &$cardPile): Hand
{
  $hand = new Hand();

  for ($i = 0; $i < 5; $i++) {
      $availableCards = $cardPile->getPile();  // Obtiene todas las cartas disponibles
        $totalCards = count($availableCards);  // Obtiene el total de cartas disponibles

        if ($totalCards == 0) {
            throw new Exception("No hay suficientes cartas en la pila.");
        }
        $randomIndex = mt_rand(0, $totalCards - 1);  // Genera un número aleatorio entre 0 y el total de cartas - 1
       
        $randomCard = $availableCards[$randomIndex];  // Recupera la carta correspondiente al índice aleatorio
        
        $hand->addCard($randomCard);  // Agrega la carta a la mano
        
        $cardPile->removeCard($randomCard);  // Elimina la carta de la pila para evitar repeticiones
    }
  
  return $hand;
  
} 

function showCards(&$hand)
{
  echo "<h3>Poker hand</h3>";
  foreach ($hand->getCards() as $card) {
      echo '<img src="' . $card->getImage() . '" alt="Card" style="max-width: 150px; margin: 5px;">';
  }

}

function getPoints(&$hand): string
{
  $points = ""; // Inicializamos una cadena vacía para almacenar los puntos

    foreach ($hand->getCards() as $card) {
        // Obtener el valor de la carta
        $value = $card->getValue();

        // Si el valor es 1 (As), se convierte a 14
      
        if ($value >= 14 && $value <= 52) {
          $value = (($value - 1) % 13) + 1;
        }
  
        if ($value == 1 || $value == 14 || $value == 27 || $value == 40) {
          $value = 14;
        }

        // Agregar el valor al string de puntos
        $points .= $value . " ";
    }

    // Eliminar el espacio adicional al final y retornar la cadena de puntos
    return trim($points);
}

function showPoints($text)
{
  echo "<br></br>";
  echo "<h2>Result</h2>";
  echo "<p>CARD POINTS: $text</p>";
}

function getTotal(&$hand): int
{
  $total = 0;

    foreach ($hand->getCards() as $card) {
        // Si el valor de la carta es 1 (As), sumamos 14
        // De lo contrario, simplemente sumamos el valor de la carta
        $total += ($card->getValue() == 1) ? 14 : $card->getValue();
    }

    return $total;
}

function showTotal($total)
{
  // Show numbers
  echo "<p>TOTAL POINTS: $total</p>";
  //TODO
}

function evaluate(&$hand)
{
  //ORDER
  //isRoyalFlush
  //isStraightFlush
  //isRepoker
  //isPoker
  //isFullHouse
  //isFlush
  //isStraight
  //isThreeOfAKind
  //isTwoPairs
  //isOnePair
  //Otherwise



  //If the hand is a isRoyalFlush print RESULT: ROYAL FLUSH

  //else if the hand is isStraightFlush print RESULT: STRAIGHT FLUSH

  //etc...

  //otherwise print RESULT: HIGH CARD

}

function isRoyalFlush($hand): bool
{

  //TODO
  return false;
}


function isStraightFlush($hand): bool
{
    //TODO
    return false;

  
}

function isRepoker($hand): bool
{
  //TODO
  return false;
}

function isPoker($hand): bool
{
    //TODO
    return false;
}

function isFullHouse($hand): bool
{
    //TODO
    return false;
}

function isFlush($hand): bool
{
    //TODO
    return false;
}

function isStraight($hand): bool
{
    //TODO
    return false;
}

function isThreeOfAKind($hand): bool
{
    //TODO
    return false;
}

function isTwoPairs($hand): bool
{
    //TODO
    return false;
}

function isOnePair($hand): bool
{
    //TODO
    return false;
}
