<?php

class card
{
    // Properties
    private int $value;
    private int $suit;
    private string $image;

    //Constructor
    public function __construct(
        int $value, 
        int $suit, 
        string $image)
    {
        $this->value = $value;
        $this->suit = $suit;
        $this->image = $image;
    }
    //Getters and setters

    function getValue(): int
    {
        return $this->value;
    }

    function getSuit(): int
    {
        return $this->suit;
    }

    function getImage(): string
    {
        return $this->image;
    }

    function setValue(int $value): void {
        $this->value = $value;
    }  

    function setSuit(int $suit): void {
        $this->suit = $suit;
    }

    function setImage(string $image): void {
        $this->image = $image;
    }

    //toString
    function __toString() {
        return 
            "Value: " . $this->getValue() .
            "Suit: " . $this->getSuit() .
            "Imatge: " . $this->getImage();
    }
}

?>