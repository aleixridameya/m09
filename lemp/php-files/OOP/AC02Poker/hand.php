<?php

class Hand
{
    private array $cards;

    // Constructor
    public function __construct()
    {
        $this->cards = array();
    }

    // Getters and setters

    function getCards(): array
    {
        return $this->cards;
    }

    function setCards(array $cards): void {
        $this->cards = $cards;
    }

    // Meyhods
    function addCard(Card $card): void
    {
        $this->cards[] = $card;
    }

    // toString
    public function __toString(){
        return    "Cards: " . $this->getCards();
    }
}

?>