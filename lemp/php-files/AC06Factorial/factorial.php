<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="style.css" />
</head>
<body>
    <?php
        include 'codi.php';
    ?>
    <div class="tot">
        <h1>Factorial Calculator: n!</h1>
        <div class="contingut">
            <p>Enter a number to calculate the factorial n! using the calculator below</p>
            <form method="POST">
                <span>Number: </span>
                <input type="integer" name="number" placeholder="Enter a number" value="<?php echo $number ?>" required>
                <br>
                <button class="boto" type="submit" name="execute" >SEND</button>            </form>
            <div class='grey'>
                <p>Factorial number</p>
                <br>
                <p> n! = <?php echo execute(); ?></p>
            </div>
        </div>
        <div class="text">
            <h4>Factorial Formulas</h4>
            <p>The formula to calculate a factorial for a number is:</p>
            <p>n! = n × (n-1) × (n-2) × … × 1</p>
            <p>Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the number 1 is reached.</p>
            <p>The factorial of zero is 1:</p>
            <p>0! = 1</p>
            <h4>Recurrence Relation</h4>
            <p>And the formula expressed using the recurrence relation looks like this:</p>
            <p>n! = n × (n – 1)!</p>
            <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recurses until n is equal to 0.</p>
            <h4>Factorial Table</h4>
            <p>The table below shows the factorial n! for the numbers one to one-hundred</p>
        </div>
        <div class="taula">
        <?php
            echo factorialTable();
        ?>
        </div>
        

        
    </div>

</body>
</html>

