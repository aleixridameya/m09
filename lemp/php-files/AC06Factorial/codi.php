

<?php

$number = ""; 

if ($_POST) {
    if (isset($_POST['execute'])) {
        $number = isset($_POST['number']) ? validate($_POST['number']) : ""; 
        $factorial = factorial($number);
    }
}



function validate($data){

    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function execute() {
    

    $fac = "";
    $num = "";
    if (isset($_POST["execute"])) {

    
    if (isset($_POST['number']) && $_POST['number'] != "") {
        $num = validate($_POST['number']);
        $fac = factorial($num);
    }


    return $fac;
    }
}

function factorial($valor): float 
{
    if ($valor == 0) return 1;

    $fac = 1;
    for ($i = $valor; $i > 1; $i--) {
        $fac *=$i;
    }

    return $fac;
}

function factorialTable() {
    echo "<table>";
    echo "<tr>";
    echo "<th>n</th>";
    echo "<th>n!</th>";
    echo "</tr>";
    
    for ($i = 1; $i <= 100 ; $i++){
        $fac = 1;
    for ($j = 1; $j <= $i ; $j++){
        $fac *=$j;
    }

    echo "<tr>";
    echo "<td>$i</td>";
    echo "<td>$fac</td>";
    echo "</tr>";
    
    }
    echo "</table>";

}

?>