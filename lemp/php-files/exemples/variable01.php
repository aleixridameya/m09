<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<head>
    <link rel="stylesheet" href="style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
    <?php
        $h1 = "Title Page";
        $txt = "<h1>$h1</h1>";
        $txt .= "<h2>Header h2</h2>"; // al posar .= afageixes el contingut a la variable $txt
        echo $txt;

        $p = "This is a very silly linw with a \$txt"; // al posar \ es mostra l'escrit literal
        echo "<p>$p</p>";

        function myfunction1() {
            $div = "This is a div";
            echo "<div>$div</div>";
            // echo "<div>$txt</div>"; No pots veure les variables fora de la funció ni al revés.
            global $div ; // Al posar **Global** ens permet utilitzar la variable dins i fora de la funció
        }

        myfunction1();
        echo $div;


        echo "<br>GLOBAL</br>";
        echo $GLOBALS['h1'];
        echo $GLOBALS['txt'];
        echo $GLOBALS['div'];
        

        function myfunction2() {
            static $i = 0; // Es posa static per conservar el valor en cas de que es cridi varies vegades
            echo $i;
            $i++;
        }

        echo "<br/>";
        myfunction2(); //Cada vegada sortirà 0 pk no guarda el valor (sense static)
        myfunction2();
        myfunction2();


        if (isset($h1)) { // si existeix la variable $i 
            echo "\$i exists";
        } else {
            echo "\h1 does not exist" , "<br/>";
        }

       
        $y = 100; //integer
        $f = 20.56345435; //float
        $t = "Text"; //str
        $b = true; //bool

        $result = sprintf("This is the result: %d, %.3f, %s, %b", $y, $f, $t, $b); // %.3f mostra nomes 3 dàcimals com a màxim
        echo "<br/>" . $result . "<br/>"; // el . serveix per concatenar
        
        echo "<br/>" . "{$y} HOLA {$b}" . "<br/>"; // les {} serveixen per posar variables dins d'una franja de text.

        $lorem = "Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas Letraset, las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.";

        echo strlen($lorem) . "<br/>";
        echo str_word_count($lorem) . "<br/>"; //conta les paraules que hi ha en la variable
        echo strrev($lorem) . "<br/>"; 
        echo strpos($lorem, "texto", 1) . "<br/>"; //busca una paraula te la variable donada un posició (opcional)
        
        
        $z = 120;
        
        echo PHP_INT_MAX . "<br/>"; // The largest integer supported (informacio en DataTypes (integer))
        echo $z * 2.80 . "<br/>"; // Multiplicació 

        echo is_int($z) . "<br/>"; //En cas de que sigui integer mostra 1
        echo is_integer($z) . "<br/>"; //En cas de que sigui integer mostra 1

        echo intval($z)  . "<br/>"; // ?
        echo PHP_FLOAT_MAX . "<br/>"; // The largest float supported (informacio en DataTypes)

        echo var_dump($z) . "<br/>"; // indica el que és "int(120)"
        
        $sin = sin(100); // fa el sinus del numero escollit

        if (!is_nan($sin)) { // The is_nan() function checks whether a value is 'not a number'.
        echo $sin . "<br/>";  
        echo var_dump($sin) . "<br/>";
        }
        
        
        
        if ($sin) { // si es bol entrara "true" Result of echoing TRUE value displays 1 while for FALSE it shows nothing.
            echo "sin is true" . "<br />";
        }else {
            echo "bol1 is false" . "<br />";
        }

        // important taula: https://classesjs.gitlab.io/asix/m09/m09-teoria/UFS/UF1/php/phpoperators/#comparison-operators
        
        $a =10;
        $b = 10;

        $pow = $a ** $b;

        echo "pow = $pow" . "<br />";
        
        $res = $a <=> $b; //Returns an integer less than, equal to, or greater than zero, depending on if $x is less than, equal to, or greater than $y. Introduced in PHP 7.
        echo $res; 

        $res2 = $a == $b ? "Equals" : "Different";
        echo $res2 . "<br />";
        
        $c = 120;
        $d = 25;

        $res3 = $a == $b ? ($c > $d ? "1" : "2") : "3"; // si a i b son iguals es realitza "($c > $d ? "1" : "2")" i si c > d mostra 1 en cas diferent mostra 2. si a i b són diferent mostrarà el 3. 
        echo $res3 . "<br />";
        

        //FunciÓ amb arguments

        //declare(strict_types=1); // És stricte amb les dades, és a dir si dius que la funció retorna un float ha de ser float si o si "no m'ha funcionat"

        function add($x1,$x2) : float  // amb els ": float" dius que la funció retorna un float
        {
            $y = $x1 + $x2;
            return $y;
        }

        echo "Add: " . add(4.3, "6.5") . "<br/>"; // es poden fer operacions encara que el numero estigui en mode text.


        function mult( &$x1, &$x2){ // el & ¿?
            return $x1 + $x2;

        }

        $a = 10;
        $b = 15;

        echo "Mult: " . add($a, $b) . "<br/>"; 
        echo "a: " . $a . "<br/>";
        echo "b: " . $b . "<br/>";


        //switch
        $favcolor = "red";

        switch ($favcolor) {
            case "red":
                echo "Your favorite color is red!";
                break;
            case "blue":
                echo "Your favorite color is blue!";
                break;
            case "green":
                echo "Your favorite color is green!";
                break;
            default:
                echo "Your favorite color is neither red, blue, nor green!";
        }
        
        echo "<br/ >";
        //loops
        $x = 1;

        //whiles
        while ($x <= 5) {
            echo "The number is: $x <br>";
            $x++;
        }

        //for 
        for ($x = 0; $x <= 10; $x++) {
            echo "The number is: $x <br>";
        }
        ?> 
</body>