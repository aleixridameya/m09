<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class ParenthesesTest extends TestCase {

   
#[Test]
#[TestDox("Parentheses")]
public function testParentheses() {

    require_once __DIR__ . "/../../src/php/Parentheses.php";

    $paren = new Parentheses();

    $this->assertTrue($paren->parentheses(""));
    $this->assertTrue($paren->parentheses("(())()"));
    $this->assertTrue($paren->parentheses("()((()))()"));
    $this->assertTrue($paren->parentheses("(())"));
    $this->assertTrue($paren->parentheses("(())(())()(())"));
    $this->assertTrue($paren->parentheses("()"));
    $this->assertFalse($paren->parentheses("("));
    $this->assertFalse($paren->parentheses("()())())"));
    $this->assertFalse($paren->parentheses(")()())()"));
    $this->assertFalse($paren->parentheses(")"));
   
  }

}
    
