<?php
$servername = "172.20.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM address a
    INNER JOIN country c ON a.country_id = c.country_id
    ORDER BY a.address_id";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<th>address_id</th>";
    echo "<th>street_number</th>";
    echo "<th>street_name</th>";
    echo "<th>city</th>";
    echo "<th>country_id</th>";
    echo "<th>country_id</th>";
    echo "<th>country_name</th>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["address_id"] . "</td>";
        echo "<td>" . $row["street_number"] . "</td>";
        echo "<td>" . $row["street_name"] . "</td>";
        echo "<td>" . $row["city"] . "</td>";
        echo "<td>" . $row["country_id"] . "</td>";
        echo "<td>" . $row["country_id"] . "</td>";
        echo "<td>" . $row["country_name"] . "</td>";
        echo "</tr>";
    }

} else {
    echo "No data found!";
}

//Close connection
echo "BD disconnected";
$conn->close();


?>