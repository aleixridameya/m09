<?php

class Country
{
    // Properties
    private int $country_id;
    private string $country_name;

    function __construct(   int $country_id, string $country_name)
    {
        $this->country_id = $country_id;
        $this->country_name = $country_name;
    }

    public function getCountryId(): int
    {
        return $this->country_id;
    }
    public function getCountryName(): string
    {
        return $this->country_name;
    }

    public function setCountryId(int $country_id): void
    {
        $this->country_id = $country_id;
    }
    public function setCountryName(string $country_name): void
    {
        $this->country_name = $country_name;
    }

    public function __toString(): string
    {
        return
        'Country_Id: ' . $this->country_id .
        'country_name: ' . $this->country_name;
    }      


}
