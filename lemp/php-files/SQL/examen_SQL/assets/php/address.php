<?php
require_once('country.php');

class Address
{
    // Properties
    private int $address_id;
    private string $street_number;
    private string $street_name;
    private string $city;
    private Country $country;

    function __construct(
        int $address_id,
        string $street_number,
        string $street_name, 
        string $city,
        Country $country)
    {
        $this->address_id = $address_id;
        $this->street_number = $street_number;
        $this->street_name = $street_name;
        $this->city = $city;
        $this->country = $country;
    }

    public function getAddressId(): int
    {
        return $this->address_id;
    }
    public function getStreetNumber(): string
    {
        return $this->street_number;
    }
    public function getStreetName(): string
    {
        return $this->street_name;
    }
    public function getCity(): string
    {
        return $this->city;
    }
    public function getCountry(): Country
    {
        return $this->country;
    } 
    
    public function setAddressId(int $address_id): void
    {
        $this->address_id = $address_id;
    }

    public function setStreetNumber(string $street_number): void
    {
        $this->street_number = $street_number;
    }

    public function setStreetName(string $street_name): void
    {
        $this->street_name = $street_name;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }
        
    public function __toString(): string
    {
        return
        'address_id: ' . $this->address_id .
        'street_number: ' . $this->street_number .
        'street_name' . $this->street_name .
        'city:' . $this->city .
        'country: ' . $this->country;
    }       

}