<?php
$servername = "172.20.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = mysqli_connect($servername, $username, $password, $dbname);
include_once("address.php");
include_once("country.php");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_GET['execute'])){
    $ciutat = $_GET['city'];

    $sql = "SELECT * FROM address a
    INNER JOIN country c ON a.country_id = c.country_id
    WHERE LOWER(a.city) LIKE '%$ciutat%'
    ORDER BY a.address_id";

    $result = $conn->query($sql);

    echo "<table class='table table-striped table-primary'>"; 
    if ($result->num_rows > 0) {
        echo "<th>address_id</th>";
        echo "<th>street_number</th>";
        echo "<th>street_name</th>";
        echo "<th>city</th>";
        echo "<th>country_id</th>";
        echo "<th>country_name</th>";
        while ($row = $result->fetch_assoc()) {
            $country = new Country($row["country_id"], $row["country_name"]);
            $address = new Address($row["address_id"], $row["street_number"], $row["street_name"], $row["city"], $country);
            echo "<tr>";
            echo "<td>" . $address->getAddressId() . "</td>";
            echo "<td>" . $address->getStreetNumber() .  "</td>";
            echo "<td>" . $address->getStreetName() . "</td>";
            echo "<td>" . $address->getCity(). "</td>";
            echo "<td>" . $country->getCountryId() . "</td>";
            echo "<td>" . $country->getCountryName() . "</td>";
            echo "</tr>";
        }

    } else {
        echo "No data found!";
    }
    echo "</table>";

}

$conn->close();
