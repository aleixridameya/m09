<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

    <link type="text/css" rel="stylesheet" href="assets/css/style.css" />
    <title>BD</title>
</head>

<body>
    <form method="GET">
        <div class='row'>
            <div class='col-md-6'>
                <input class='form-control' name='city' placeholder='Enter a city'>
            </div>
        <div class='col-md-6'>
            <button class='btn btn-primary' type='submit' name='execute'>Search</button>
        </div>
        </div>
    </form>
    <?php include 'assets/php/allAddressesByCity.php'; ?>
</body>

</html>