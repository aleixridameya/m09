<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </head>
</html>
<?php
$servername = "172.20.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: ". $conn->connect_error); 
}

$sql = "SELECT book_id, title, isbn13, publication_date FROM book";

$result = $conn->query($sql);

echo "<table class='table table-stripped'>";

if ($result->num_rows > 0) {
    echo "<th>ID</th>";
    echo "<th>TITLE</th>";
    echo "<th>ISBN13</th>";
    echo "<th>PUBL.DATE</th>";
    while ($row = $result->fetch_assoc()) {    
      echo "<tr>"; 
      echo "<td>" . $row["book_id"] . "</td>";
      echo "<td>" . $row["title"] . "</td>";
      echo "<td>" . $row["isbn13"] . "</td>";
      echo "<td>" . $row["publication_date"] . "</td>"; 
      echo "</tr>";
    }
}

echo "</table>";

?>