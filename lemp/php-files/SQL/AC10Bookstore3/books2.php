<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
        <h1>Books by TITLE</h1>
    <div class="container">
    <form method="GET">
        <div class='row'>
            <div class='col-md-6'>
                <input class='form-control' name='title' placeholder='Enter a title'>
            </div>
        <div class='col-md-6'>
            <button class='btn btn-primary' type='submit' name='execute' >Search</button>
        </div>
        </div>
    </form>
    </div>
    </body>
</html>

<?php
$servername = "172.18.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error){
    die("Connection failed: " . $conn->connect_error);
}

function execute($conn, $title){
    $stmt = $conn->prepare("CALL get_all_books_by_title(?)");
    $stmt->bind_param("s", $title);
    $stmt->execute();
    $result = $stmt->get_result();

    echo "<table class='table table-striped table-primary'>"; 
    echo "<tr class='table-success'>";
    echo "<td>ID</td>";
    echo "<td>TITLE</td>";
    echo "<td>ISBN13</td>";
    echo "<td>NUM_PAGES</td>";
    echo "<td>PUBL DATE</td>";
    echo "<td>PUBLISHER</td>";
    echo "<td>LANGUAGE</td>";
    echo "<td>AUTHOR</td>";
    echo "</tr>";
    
    if ($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            echo "<tr>";
            echo "<td>" . $row["book_id"] . "</td>";
            echo "<td>" . $row["title"] . "</td>";
            echo "<td>" . $row["isbn13"] . "</td>";
            echo "<td>" . $row["num_pages"] . "</td>";
            echo "<td>" . $row["publication_date"] . "</td>";
            echo "<td>" . $row["publisher_name"] . "</td>";
            echo "<td>" . $row["language_name"] . "</td>";
            echo "<td>" . $row["author_name"] . "</td>";
            echo "</tr>";
        }
    } 
    $stmt->close();
}

if(isset($_GET['execute'])){
    $title = "%" . $_GET['title'] . "%";
    execute($conn, $title);
}

$conn->close();
?>
