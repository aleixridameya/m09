<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
        <h1>Books by TITLE</h1>
    <div class="container">
    <form method="GET">
        <div class='row'>
            <div class='col-md-6'>
                <input class='form-control' name='search' placeholder='Enter a title'>
            </div>
        <div class='col-md-6'>
            <button class='btn btn-primary' type='submit' name='execute' >Search</button>
        </div>
        </div>
    </form>
    </div>
    </body>
</html>

<?php
$servername = "172.18.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: ". $conn->connect_error); 
}

if (isset($_GET['execute'])){
    $search = $_GET['search'];


    $sql = "SELECT * FROM book b
    INNER JOIN book_language bl ON b.language_id = bl.language_id
    INNER JOIN publisher p ON b.publisher_id = p.publisher_id
    INNER JOIN book_author ba ON b.book_id = ba.book_id
    INNER JOIN author a ON a.author_id = ba.author_id
    WHERE LOWER(title) LIKE '%$search%'
    ORDER BY b.book_id";

    $result = $conn->query($sql);

    echo "<table class='table table-stripped'>";

    if ($result->num_rows > 0) {
        echo "<th>ID</th>";
        echo "<th>TITLE</th>";
        echo "<th>ISBN13</th>";
        echo "<th>PUBL.DATE</th>";
        echo "<th>NUM_PAGES</th>";
        echo "<th>PUBLISHER</th>";
        echo "<th>LANGUAGE</th>";
        echo "<th>AUTHOR</th>";
        while ($row = $result->fetch_assoc()) {    
          echo "<tr>"; 
          echo "<td>" . $row["book_id"] . "</td>";
          echo "<td>" . $row["title"] . "</td>";
          echo "<td>" . $row["isbn13"] . "</td>";
          echo "<td>" . $row["publication_date"] . "</td>";
          echo "<td>" . $row["num_pages"] . "</td>";
          echo "<td>" . $row["publisher_name"] . "</td>";
          echo "<td>" . $row["language_name"] . "</td>";
          echo "<td>" . $row["author_name"] . "</td>";
          echo "</tr>";
        }
    echo "</table>";
}}


?>