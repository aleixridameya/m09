<?php
class Book
{
    // Properties
    private int $book_id;
    private string $title;
    private string $isbn13;
    private Language $language;
    private int $num_pages;
    private string $publication_date;
    private Publisher $publisher;
    private array $authors; //This is an array of Author object class
    // Constructor
    public function __construct(
        int $book_id,
        string $title,
        string $isbn13,
        Language $language,
        int $num_pages,
        string $publication_date,
        Publisher $publisher,
        array $authors)
    {
        $this->book_id = $book_id;
        $this->title = $title;
        $this->isbn13 = $isbn13;
        $this->language = $language;
        $this->num_pages = $num_pages;
        $this->publication_date = $publication_date;
        $this->publisher = $publisher;
        $this->authors = $authors;
    }
    // Getters and setters
    function setBookId(int $book_id): void {
        $this->book_id = $book_id;
    }
    
    function setTitle(string $title): void {
        $this->title = $title;
    }

    function setISBN13(string $isbn13): void {
        $this->isbn13 = $isbn13;
    }

    function setLanguage(Language $language): void {
        $this->language = $language;
    }

    function setNumPages(int $num_pages): void {
        $this->num_pages = $num_pages;
    }

    function setPublicationDate(string $publication_date): void {
        $this->publication_date = $publication_date;
    }

    function setPublisher(string $publisher): void {
        $this->publisher = $publisher;
    }

    function setAuthors(array $authors): void {
        $this->authors = $authors;
    }

    function getbook_id(): int
    {
        return $this->book_id;
    }

    function getTitle(): string
    {
        return $this->title;
    }

    function getISBN13(): string
    {
        return $this->isbn13;
    }

    function getLanguage(): Language
    {
        return $this->language;
    }

    function getNumPages(): int
    {
        return $this->num_pages;
    }

    function getPublicationDate(): string
    {
        return $this->publication_date;
    }

    function getPublisher(): Publisher
    {
        return $this->publisher;
    }

    function getAuthors(): array
    {
        return $this->authors;
    }


    // Methods
    public function add_author(Author $author): void {
        $this->authors[] = $author;
    }
    
    public function remove_author(Author $author): void {
        $index = array_search($author, $this->authors);
        if ($index != false){
            unset($this->authors[$index]);
        }
    }
    //toString
    public function __toString(): string
    {
        return 
        "Value: " . $this->getbook_id() .
        "Title: " . $this->getTitle() .
        "ISBN13: " . $this->getISBN13() .
        "Language: " . $this->getLanguage() .
        "NumPages: " . $this->getNumPages() .
        "PublicationDate: " . $this->getPublicationDate() .
        "Publisher: " . $this->getPublisher() .
        "Authors: " . $this->getAuthors();
    }

}

?>