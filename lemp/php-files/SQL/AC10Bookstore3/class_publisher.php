<?php
class Publisher {
    // Properties
    private int $publisher_id;
    private string $publisher_name;
    
    // Constructor
    function __construct(
        int $publisher_id, 
        string $publisher_name) 
    {
        $this -> publisher_id = $publisher_id;
        $this -> publisher_name = $publisher_name;
    }

    // Getters and setters
    function get_publisher_id(): int{
        return $this -> publisher_id;
    }

    function set_publisher_id(int $publisher_id): void {
        $this -> publisher_id = $publisher_id;
    }

    function get_publisher_name(): string{
        return $this -> publisher_name;
    }

    function set_publisher_name(string $publisher_name): void {
        $this -> publisher_name = $publisher_name;
    }

    //toString
    
    public function __toString(): string {
        return "Publisher:\n" . 
        "Publisher_id: " . $this->get_publisher_id() .
        "Publisher_name: " . $this->get_publisher_name();
    }
}
