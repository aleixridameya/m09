<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
        <h1>Books by TITLE</h1>
    <div class="container">
    <form method="GET">
        <div class='row'>
            <div class='col-md-6'>
                <input class='form-control' name='search' placeholder='Enter a title'>
            </div>
        <div class='col-md-6'>
            <button class='btn btn-primary' type='submit' name='execute' >Search</button>
        </div>
        </div>
    </form>
    </div>
    </body>
</html>

<?php
$servername = "172.18.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

include_once('class_book.php');
include_once('class_language.php');
include_once('class_publisher.php');
include_once('class_author.php');



$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: ". $conn->connect_error); 
}
if(isset($_GET['execute'])){
    $title = "%" . $_GET['search'] . "%";
    execute($conn, $title);
}

function execute($conn, $title){
    $sql = "SELECT *
    FROM book b
    INNER JOIN book_language bl ON b.language_id = bl.language_id
    INNER JOIN publisher p ON b.publisher_id = p.publisher_id
    INNER JOIN book_author ba ON b.book_id = ba.book_id
    INNER JOIN author a ON a.author_id = ba.author_id
    WHERE LOWER(title) LIKE ?
    ORDER BY b.book_id";

    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $title);
    $stmt->execute();
    $result = $stmt->get_result();

    $books = array();

    while($row = $result->fetch_assoc()){
        $language = new Language($row["language_id"], $row["language_code"], $row["language_name"]);
        $publisher = new Publisher($row["publisher_id"], $row["publisher_name"]);
        $author = new Author($row["author_id"], $row["author_name"]);

        $book_id = $row['book_id'];
        if (!isset($books[$book_id])) {
            $books[$book_id] = new Book(
                $row['book_id'],
                $row['title'],
                $row['isbn13'],
                $language,
                $row['num_pages'],
                $row['publication_date'],
                $publisher,
                array($author)
            );
        } else {
            $books[$book_id]->add_author($author);
        }
        }

    echo "<table class='table table-striped table-primary'>"; 
    echo "<tr class='table-success'>";
    echo "<td>ID</td>";
    echo "<td>TITLE</td>";
    echo "<td>ISBN13</td>";
    echo "<td>NUM_PAGES</td>";
    echo "<td>PUBL DATE</td>";
    echo "<td>PUBLISHER</td>";
    echo "<td>LANGUAGE</td>";
    echo "<td>AUTHOR</td>";
    echo "</tr>";

    foreach ($books as $book) {
        $authors = $book->getAuthors();
        echo "<tr>";
        echo "<td>" . $book->getbook_id() . "</td>";
        echo "<td>" . $book->getTitle() . "</td>";
        echo "<td>" . $book->getISBN13() . "</td>";
        echo "<td>" . $book->getNumPages() . "</td>";
        echo "<td>" . $book->getPublicationDate() . "</td>";
        echo "<td>" . $book->getPublisher()->get_publisher_name() . "</td>";
        echo "<td>" . $book->getLanguage()->getLanguageName() . "</td>";
        echo "<td>";
        foreach ($authors as $key => $author) {
            echo $author->get_author_name();
            if ($key < count($authors) - 1) {
                echo ", ";
            }
        }
        echo "</td>";
        echo "</tr>";
    }

echo "</table>";
$stmt->close();
}



$conn->close();



?>