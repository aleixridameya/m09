<?php
class Library {
    // Properties
    private string $name;
    private array $books;

    // Constructor
    public function __construct(string $name){
        $this -> name = $name;
    }

    // Getters and setters
    function get_name(): string{
        return $this -> name;
    }

    function set_name(string $name): void {
        $this -> name = $name;
    }

    function get_books(): array{
        return $this->books;
    }

    function set_books(array $books): void {
        $this->books=$books;
    }

    // Methods
    public function add_book(Book $book): void{
        $this->books[] = $book;
    }

    public function get_books_by_title(string $title): array {
        $resultat = [];
        foreach ($this->books as $book){
            if ($book->get_title() == $title){
                $result[] = $book;
            }
        }
        return $result;
    }

    public function get_books_by_isbn13(string $isbn13): array {
        $resultat = [];
        foreach ($this->books as $book){
            if ($book->get_isbn13() == $isbn13){
                $result[] = $book;
            }
        }
        return $result;
    }

    public function get_books_by_author_name(string $author_name): array {
        $resultat = [];
        foreach ($this->books as $book){
            if ($book->get_name() == $author_name){
                $result[] = $book;
            }
        }
        return $result;
    }
    
    //toString
    public function __toString(): string
    {
        $booksString = "";
        foreach ($this->books as $book) {
            $booksString .= $book . "\n\n";
        }

        return "Library: " . $this->name . "\n\n" .
            "Books:\n" . $booksString;
    }
}

?>