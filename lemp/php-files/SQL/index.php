<?php
$servername = "172.20.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: ". $conn->connect_error); 
} else {
    echo "Connected successfully";
}

//SELECT

//$sql = "INSERT INTO book ( book_id, title, isbn13, language_id, num_pages, publication_date, publisher_id )
  //      VALUES (?,?,?,?,?,?,?)";


$sql = "UPDATE book SET title = ? WHERE book_id = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("issiisi", $id, $title, $isbn, $language_id, $num_pages, $publication_date, $publisher_id);


$title = "This is my new title";
$book_id = 11129;

/*
$id = 11129;
$title = "New Book";
$isbn = "111111" ;
$language_id = "1";
$num_pages = 100;
$publication_date = "2023-05-03";
$publisher_id = 1;
$stmt->execute();
*/

$result = $stmt->get_result();

if ($stmt->affected_rows > 0) {
//if ($result->num_rows > 0) {
    //echo "Num record: " . $result->num_rows . "<br>"  ;
    echo "<br> Num rows inserted: " . $stmt->affected_rows;
    //while ($row = $result->fetch_assoc()) {    
      //echo $row["book_id"] ." - ". $row["title"] . " - " . $row["isbn13"] . " - " . $row["num_pages"] . "<br>";
      //  echo "book_id: " . $row["book_id"] . " - title: " . $row["title"] . ", " . $row["isbn13"] . ", " . $row["num_pages"] . "<br>";
    //}
} else {
    echo "no data found";
}

$conn->close();


?>