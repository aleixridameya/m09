<?php

echo '<h1>Animes</h1>';

$url = "data.json";

$data = file_get_contents($url); 
$array = json_decode($data, true); 

echo "<div>";
$cont = 0;
$genre = array();
$year = array();
    foreach ($array as $item) {
        foreach ($item as $key => $value) {
            $cont +=1;
            $only2 = explode(",",$value['genre']);
            foreach ($only2 as $valor) {
                    array_push($genre, $valor);
            }
            array_push($year, $value['year']);

            }
        }
    



    sort($year);
    sort($genre);
    $genre_list = array_unique($genre);
    $year_list = array_unique($year);

    echo "Num animes: $cont <br>" ;
    echo "<br>";

    echo "Genres:";
        foreach ($genre_list as $gen) {
            echo $gen;
        }
    echo "<br>";
    echo " <br> Year:" ;
        foreach ($year_list as $any) {
            echo " " . $any;
        } 
echo "<br>";
echo "</div>";



echo "<table>";
foreach ($array as $item) {
    foreach ($item as $key => $value) {
            echo "<tr>";
                echo "<td rowspan='6'> <img src='https://joanseculi.com/$value[image]'> </td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td> ID: $value[id]</td>";
                echo "<td> Type: $value[type]</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td> Name: $value[name]</td>";
                echo "<td> Year: $value[year]</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td> Original Name: $value[originalname]</td>";
                echo "<td> Rating: $value[rating]</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td> Demography: $value[demography]</td>";
                echo "<td> Genre: $value[genre]</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td colspan='2'> Synopisis: <br><br> $value[description]</td>";
            echo "</tr>"; 
    }

echo "</table>";
}




?>