<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
        <?php
            include 'codi.php';
        ?>
        <div class="container mt-3 ms-3">
        <h2 >ROLL A NUMBER OF DICE</h2>
        <form method="POST">
        <span >Number:</span>
            <br>
            <input class="number" type="number" id="numero" name="number" placeholder="Enter a number" value="<?php echo isset($_POST['number']) ? $_POST['number'] : ''; ?>" required>
            <br>
            <br>
            <button class="btn btn-primary" type="submit" name="execute" >ROLL</button>
            <br>
        </form>
        <p>Click the button to roll a number of dice</p>
        <p> <?php echo execute(); ?></p>
        </div>
    </body>
</html>