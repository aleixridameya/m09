<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="style.css" />
</head>
<body>
    <?php
        include 'codi.php';
    ?>
    <div class="tot">
        <h1>Exponent Calculator</h1>
        <div class="contingut">
            <br>
            <p>Exponent Calculator</p>
            <div class='grey'>
            <br>
            <form method="POST">
                <span>x =  </span>
                <input type="number" step="any" name="number" placeholder="Enter a number" value="<?php echo isset($_POST['number']) ? $_POST['number'] : ''; ?>" required>
                <br>
                <br>
                <span>n =  </span>
                <input type="number" step="any" name="n_number" placeholder="Enter a number" value="<?php echo isset($_POST['n_number']) ? $_POST['n_number'] : ''; ?>" required>
                <br><br>
                <button class="boto" type="submit" name="execute" >CALCULATE</button>
                </form>
                <p>Exponent Calculator</p>
                
                <p> n! = <?php echo execute(); ?></p>
            </div>
        </div>
        <div class="text">
            <h4>Positive exponent</h4>
            <p>Exponentiation is a mathematical operation, written as xn, involving the base x and an
                exponent n. In the case where n is a positive integer, exponentiation corresponds to
                repeated multiplication of the base, n times.</p>
            <p> \[x^2 = x · x · ......· x\]</p>
            <p>\[n \space times\]</p>
            <p>The calculator above accepts negative bases, but does not compute imaginary numbers. It
                also does not accept fractions, but can be used to compute fractional exponents, as long
                as the exponents are input in their decimal form</p>
            <h4>Negative exponent</h4>
            <p>When an exponent is negative, the negative sign is removed by reciprocating the base and
                raising it to the positive exponent.
            </p>
            <p> \[x^n = {1 \over x · x ......· x}\]</p>
            <p>\[n \space times\]</p>
            <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recurses
                until n is equal to 0</p>
            <h4>Exponent 0</h4>
            <p>When an exponent is 0, any number raised to the 0 power is 1.</p>
            <p> \[x^n = 1\]</p>
            <p>
                For 0 raised to the 0 power the answer is 1 however this is considered a definition and not
                an actual calculation
            </p>

            <br/>
            <h4>Real number exponent</h4>
            <p>For real numbers, we use the PHP function pow(n,x).</p>
        </div>
    </div>

</body>
</html>